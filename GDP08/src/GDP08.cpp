//============================================================================
// Name        : GrupoDeProblemas08.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdlib.h>
#include <cmath>
using namespace std;

struct vector{
	int *elementos;
	int cantidad;
};

void problema01();
int* problema02(int cantidad, int maximo);
void imprimir(int s[],int n);


int main() {

	problema01();



	return 0;

}
void imprimir(int s[],int n){
	for(int i=0;i<n;i++){
		cout<<"Valor "<<i<<" :"<<s[i]<<endl;
	}
}

void problema01(){
	int entero;
	float real;
	int* ptr=new int[10];

}

int* problema02(int cantidad, int maximo){
	int *valores=new int[cantidad];

	for(int i=0;i<cantidad;i++){
		*valores=1.0*rand()/RAND_MAX*maximo;
		valores++;
	}
	valores-=cantidad;
	return valores;
}


int convertirDecimal(int* ptr,int b, int n){
	int respuesta=0;
	for(int i=0;i<n;i++){
		respuesta=respuesta+ (*ptr)*pow(b,n-i-1);
		ptr++;
	}
	return respuesta;
}

struct vector *convertirNumero(int numero){
	struct vector *Respuesta=new vector();
	int cantidad=log10(numero);
	cantidad++;
	int *respuesta=new int[cantidad];
	respuesta+=cantidad-1;
	for(int i=0;i<cantidad;i++){
		*respuesta=numero%10;
		numero/=10;
		respuesta--;
	}
	respuesta++;
	Respuesta->elementos=respuesta;
	Respuesta->cantidad=cantidad;

	return Respuesta;
}

struct vector *ConvertirVectorDecimal(int *numero,int n,int b){
	int convertido=convertirDecimal(numero,n,b);
	return convertirNumero(convertido);
}

struct vector *fusionarVectores( struct vector *v1,struct vector *v2){
	struct vector *Rpta=new vector();

	int *v3=new int(v1->cantidad+v2->cantidad);
	int vr3Ptr=v3;
	int *tmp=v1->elementos;

	for(int i=0;i<v1->elementos;i++){
		*vr3Ptr=*tmp;
		tmp++;
		vr3Ptr++;
	}
	tmp=v2->elementos;
	for(int i=0;i<v2->elementos;i++){
		*vr3Ptr=*tmp;
		tmp++;
		vr3Ptr++;
	}
	Rpta->cantidad=v3;
	Rpta->elementos=v1->elementos+v2->elementos;

	return Rpta;
}

void imprimirMatriz(int **matriz,int filas, int columnas){
	for(int i=0;i<filas;i++){
		for(int j=0;j<columnas;j++){
			cout<<*(*(matriz+i)+j)<<" ";
		}
		cout<<endl;
	}
}

int **obtenerMatrizIdentidad(int orden){
	int** matriz=new int*[orden];
	int* matrizPtr=matriz;
	int valor;
	for(int i=0;i<orden;i++){
		*(matrizPtr+i)=new int*[orden];
		for(int j=0;j<orden;j++){
			if(i==j){
				valor=1;
			}else{
				valor=0;
			}
			*(*(matriz+i)+j)=valor;
		}

	}
	return matriz;
}


