//============================================================================
// Name        : SolucionarioFinal.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================



#include <iostream>
#include <cmath>
#include <vector>
#include<sstream>
#include<fstream>
using namespace std;
struct texto{
	string palabra;
	struct texto *siguiente;
	struct texto *anterior;
};
struct cliente{
	int orden;
	int cod;
	float tip;
	float num;
	string nombre;
	struct cliente *siguiente;
};
struct libro{
	int idLibro;
	string ISBN;
	int idAutor;
	string fecha;
	struct libro *siguiente;
};
struct Cliente{
	int id_cliente;
	string nombre;
	string apellido;
	string fechaNacimiento;
	string nombreUsuario;
};
struct Transaccion{
	int idUsuario;
	int idTarjetaCredito;
	string tipoOperacion;
	string moneda;
	int monto;
	string fecha;
	string hora;
	void inicializar(int idU,int idT,string to,string coin,int money, string date, string hour){
		idUsuario=idU;
		idTarjetaCredito=idT;
		tipoOperacion=to;
		moneda=coin;
		monto=money;
		fecha=date;
		hora=hour;
	}
	void imprimir(){
		cout<<idUsuario;
		cout<<idTarjetaCredito;
		cout<<tipoOperacion;
		cout<<moneda;
		cout<<monto;
		cout<<fecha;
		cout<<hora;
	}
};
struct Repartidor{
	string nombre;
	string apellidoPaterno;
	string apellidoMaterno;
	string correo;
	int celular;
	void imprimir(){
		cout<<"**********"<<endl;
		cout<<"Nombre: "<<nombre<<endl;
		cout<<"Apellidp Paterno: "<<apellidoPaterno<<endl;
		cout<<"Apellido Materno: "<<apellidoMaterno<<endl;
		cout<<"**********"<<endl;
	}
};
struct RepartidorNuevo{
	string nombre;
	string apellidoPaterno;
	string apellidoMaterno;
	string correo;
	int celular;
	string nuevoOrdenApellido;
	void nuevoApellido(){
		nuevoOrdenApellido=apellidoMaterno+" "+apellidoPaterno;
		cout<<" "<<nuevoOrdenApellido<<endl;
	}
};

void problema11();
string insertarPalabra();
void verTextoCompleto(texto *inicio);
void eliminarUltimaPalabra(texto *inicio);
void agregarPalabraInicio(texto *inicio,string palabra);
texto *crearListaTexto();
void insertarCola(struct cliente *frente, struct cliente *fin, int orden, int cod, string nombre,float tip, float num);
bool colaVacia(cliente *frente);
void eliminarElementosCola(struct cliente *frente, struct cliente *fin);
void Problema11();
void problema01(int n, int k);
string problema02(int numero);
void problema03(int numero,int k,vector <int> ceros);
void probarProblema03();
void Problema05(char matriz[16][16]);
char invertirBomca(char *c);
void probarProblema07();
void probarProblema08a();
void Problema08b(struct RepartidorNuevo r);
void probarProblema08b();
void Problema09(vector<struct Transaccion>t,int usuario, int tarjeta, string moned);
void buscarFiltro(int filtroI,string filtroS, string nombreArchivo);
int buscarCantidadPrestamos(int filtroI,string filtroS, string nombreArchivo);




int main() {


	return 0;
}





//buscarFiltro es una funcion creada por mi basada en el grupo de problemas 4
//que me sirvio de ayuda para hacer mi parte respectiva del trabajo grupal(EZMarket)committed 2b5c21a
void buscarFiltro(int filtroI,string filtroS, string nombreArchivo){
			ifstream archivo1(nombreArchivo.c_str());
			string linea,campo1;
			int i=1;
			if(archivo1.is_open()){
				while(!archivo1.eof()){
					getline(archivo1,linea);
					stringstream s(linea);
					while(!s.eof() ){
						getline(s,campo1,';');
						if(i==filtroI){
							if(campo1==filtroS){
								cout<<linea<<endl;
							}
						}
							i++;
					}
					i=1;
				}
				archivo1.close();
			}else{
				cout<<"No se pudo abrir el archivo"<<endl;
			}
}

void aniadirDevolucion(int filtroI,string filtroS, string nombreArchivo,string dato){
	ifstream archivo1(nombreArchivo.c_str());
	string linea,campo1;
	int i=1;
	if(archivo1.is_open()){
		while(!archivo1.eof()){
			getline(archivo1,linea);
			stringstream s(linea);
			while(!s.eof() ){
				getline(s,campo1,';');
				if(i==filtroI){
					if(campo1==filtroS){
						//archivo1<<dato;
					}
				}
					i++;
			}
			i=1;
		}
		archivo1.close();
	}else{
		cout<<"No se pudo abrir el archivo"<<endl;
	}
}
int buscarCantidadPrestamos(int filtroI,string filtroS, string nombreArchivo){
			ifstream archivo1(nombreArchivo.c_str());
			string linea,campo1;
			int i=1;
			int cantidadPrestamos=0;
			if(archivo1.is_open()){
				while(!archivo1.eof()){
					getline(archivo1,linea);
					stringstream s(linea);
					while(!s.eof() ){
						getline(s,campo1,';');
						if(i==filtroI){
							if(campo1==filtroS){
								//cout<<linea<<endl;
								cantidadPrestamos=cantidadPrestamos+1;
							}
						}
							i++;
					}
					i=1;
				}
				archivo1.close();
			}else{
				cout<<"No se pudo abrir el archivo"<<endl;
			}
		return cantidadPrestamos;
}
void problema01(int n, int k){
	int cantidad=log10(n);
	if(cantidad+1==k){
		cout<<"Posible"<<endl;
	}else{
		cout<<"Imposible"<<endl;
	}
}

string problema02(int numero){
	string descripcion;
	if(numero==20){
		descripcion="FTP-Data";
	}else if(numero==21){
		descripcion="FTP-Control";
	}else if(numero==25){
		descripcion="SMTP";
	}else if(numero==53){
		descripcion="DNS";
	}else if(numero==80){
		descripcion="HTTP";
	}else{
		descripcion="resultado no encontrado";
	}
	return descripcion;
}

void problema03(int numero,int k,vector <int> ceros){
	vector <int> inverso;
	while(numero>0){
		inverso.push_back(numero%10);
		numero=numero/10;
	}
	for(int i=inverso.size()-1;i>=0;i--){
		ceros.push_back(inverso[i]);
	}
	for(int i=0;i<inverso.size();i++){
			cout<<ceros[i];
		}
}

void probarProblema03(){
	vector<int> ceros;
	problema03(100,3,ceros);
}

char invertirBomca(char *c){
	if(*c=='B'){
		*c='0';
	}
	return *c;
}

void Problema05(char matriz[16][16]){
	for(int i=0;i<16;i++){
		for(int j=0;i<16;j++){
			invertirBomca(&matriz[i][j]);
		}
	}
}


//hola mundo

void probarProblema07(){
	struct Repartidor r;
	r.nombre="Alejandro";
	r.apellidoPaterno="Araujo";
	r.apellidoMaterno="Inca";
	r.imprimir();
}

void probarProblema08a(){
	struct RepartidorNuevo r;
	r.nombre="Alejandro";
	r.apellidoPaterno="Araujo";
	r.apellidoMaterno="Inca";
	r.nuevoApellido();
}

void Problema08b(struct RepartidorNuevo r){
	cout<<r.nombre;
	r.nuevoApellido();
}

void probarProblema08b(){
	struct RepartidorNuevo r;
	r.nombre="Alejandro";
	r.apellidoPaterno="Araujo";
	r.apellidoMaterno="Inca";
	Problema08b(r);
}
void Problema09(vector<struct Transaccion>t,int usuario, int tarjeta, string moned){
	int montoAcumulado1=0;
	for(int i=0;i<t.size();i++){
		if(usuario==t[i].idUsuario and moned==t[i].moneda and tarjeta==t[i].idTarjetaCredito){
			t[i].imprimir();
			montoAcumulado1+=t[i].monto;
		}
	}
	cout<<"Usuario: "<<usuario;
	cout<<"Cuenta: "<<tarjeta;
	cout<<"Monto en total: "<<montoAcumulado1;
}

void ConsultaClientes(){
	string nombreArchivo1="clientes.txt";

	string filtroS;
	int filtroI;
	cout<<"Buscar por: "<<endl;
	cout<<"(1)ID Cliente"<<endl;
	cout<<"(3)Apellido"<<endl;
	cin>>filtroI;
	cout<<"Escriba lo que desee buscar: "<<endl;
	cin>>filtroS;
	buscarFiltro(filtroI,filtroS,nombreArchivo1);
	//Hasta que si muestran los campos del cliente, como su nombre, id, apellido...


	string nombreArchivo2="prestamos.txt";


	cout<<"La cantidad de prestamos es: "<<buscarCantidadPrestamos(2,filtroS,nombreArchivo2);
	cout<<endl;
	//con esto se muestra la cantidad de prestamos hecha

	string nombreArchivo3="contacto.txt";

	buscarFiltro(2,filtroS,nombreArchivo3);
	//con esto se imprime los campos del archivo "contacto.txt" para poder ver los
	//los datos de contacto del cliente




	//opcion Ver prestamos
	string nombreArchivo4="prestamos.txt";

	buscarFiltro(5,"",nombreArchivo4);
	//el cinco, nos indica el campo en el que se buscar� del registro "prestamo.txt", y las comillas, nos indican
	//que lo que se buscar� son espacios vac�os

	//registrar devolucion
	string nombreArchivo5="prestamos";
	cout<<"insterte la fecha"<<endl;
			string dato;
	cin>>dato;
	//aniadirDevolucion(i,string filtroS, string nombreArchivo,string dato)
}

//Problema11
//codigo obtenido de la pagina de Programacion ATS para la implementacion de una cola,
//as� como tambi�n poder agregar(registrar) y eliminar(atender) elementos a a la cola
bool colaVacia(cliente *frente){
	return (frente==NULL)? true: false;
}
//parte b
void insertarCola(struct cliente *frente, struct cliente *fin, int orden, int cod, string nombre,float tip, float num){
	cliente *nuevoNodo=new cliente();
	nuevoNodo->cod=cod;
	nuevoNodo->nombre=nombre;
	nuevoNodo->num=num;
	nuevoNodo->orden=orden;
	nuevoNodo->tip=tip;
	nuevoNodo->siguiente=NULL;
	if(colaVacia(frente)){
		frente=nuevoNodo;
	}else{
		fin->siguiente=nuevoNodo;
	}
	fin=nuevoNodo;
	cout<<nuevoNodo->cod;
	cout<<nuevoNodo->nombre;
	cout<<nuevoNodo->num;
	cout<<nuevoNodo->orden;
	cout<<nuevoNodo->tip;

}
//parte c
void eliminarElementosCola(struct cliente *frente, struct cliente *fin){
	cliente *auxiliar=frente;
	cout<<"Los datos del cliente atendido son: "<<endl;
	cout<<frente->cod<<endl;
	cout<<frente->nombre<<endl;
	cout<<frente->num<<endl;
	cout<<frente->orden<<endl;
	cout<<frente->tip<<endl;

	if(frente==fin){
		frente=NULL;
		fin==NULL;
	}else{
		frente=frente->siguiente;
	}
	delete auxiliar;
}
void Problema11(){
	cliente *frente=NULL;
	cliente *fin=NULL;

}
//Problema15 del grupo de problemas 10
//en vez de una pila se esta usando una lista doblemente enlazada porque
//en el problema pide leer desde la palabra m�s antigua hasta la m�s reciente
//y eso no se puede lograr con una pila, a lo mas se podr�a
//mostrar las palabras de la m�s reciente a la m�s antigua
texto *crearListaTexto(){
	texto *inicio=new texto();
	inicio->palabra="-1";
	inicio->siguiente=NULL;
	inicio->anterior=NULL;
	return inicio;
}
//parte a
void agregarPalabraInicio(texto *inicio,string palabra){
	texto *nuevoNodo=new texto();
	nuevoNodo->palabra=palabra;
	nuevoNodo->siguiente=inicio->siguiente;
	inicio->siguiente=nuevoNodo;
}
//parte c.ii
void eliminarUltimaPalabra(texto *inicio){
	texto *nodoEliminar=NULL;
	texto *tmp=inicio;
	tmp=tmp->siguiente;
	nodoEliminar=tmp->siguiente;
	tmp->siguiente=nodoEliminar->siguiente;
	delete nodoEliminar;
}
//parte c.i
void verTextoCompleto(texto *inicio){
	texto *tmp=inicio;
	while(tmp!=NULL){
		tmp=tmp->siguiente;
	}
	while(tmp->palabra!="-1"){
		cout<<tmp->palabra<<" "<<endl;
		tmp=tmp->anterior;
	}
}
//parte a
string insertarPalabra(){
	string palabra;
	cout<<"Digite la palabra que desea agregar"<<endl;
	cin>>palabra;
	//parte b, presionar enter para insertar el string "palabra"
	return palabra;
}

void problema11(){
	struct texto *inicio=crearListaTexto();
	string palabra=insertarPalabra();
	cout<<"Opciones"<<endl;
	cout<<"(1)Ver Texto Completo "<<endl;
	cout<<"(2)Deshacer "<<endl;
	cout<<"(3)Continuar Escribiendo "<<endl;
	int opcion;
	cin>>opcion;
	switch(opcion){
	case 1:verTextoCompleto(inicio);
		problema11();
		break;
	case 2:eliminarUltimaPalabra(inicio);
		break;
	case 3:agregarPalabraInicio(inicio,insertarPalabra());
		problema11();
		break;
	}
}



