//============================================================================
// Name        : GrupoDeProblemas4.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include<cstring>
#include<cstdlib>
#include<vector>
#include<algorithm>
#include<sstream>
using namespace std;


struct estudiante{
			int codigo;
			string nombre;
			int nota1;
			int nota2;
			int nota3;
			int nota4;
			void inicializar(int n1, int n2, int n3, int n4){
						nota1=n1;
						nota2=n2;
						nota3=n3;
						nota4=n4;
						promedio=(n1+n2+n3+n4)/4.0;
			}
			float promedio;
			void imprimir(){
				cout<<"Estudiante "<<codigo<<"tiene promedio igual a: "<<promedio<<endl;
			}

		};
void pruebaEstructuraAlumno(){
		estudiante e1,e2;
		e1.inicializar(10,12,13,2);
		e2.inicializar(4,5,5,6);

		e1.imprimir();
		e2.imprimir();
		}
void leerArchivoCPP(){
		string nombreArchivo= "archivo1.csv";
		ifstream archivo(nombreArchivo.c_str());

		if (archivo.is_open()){
			string linea;
			int contador=0;
			const int TAMANO_PAGINA=20;
			cout<<"Archivo abierto correctamente"<<endl;
			while(!archivo.eof()){
				getline(archivo,linea);
				cout<<linea<<endl;
				contador++;
				if(contador%TAMANO_PAGINA==0){
					cout<<"presione cualquier tecla para continuar"<<endl;
					cin.get();
				}
			}
			archivo.close();
		}
}
void leerArchivoAlumno(){
	string nombreArchivo= "archivo1.csv";
	ifstream archivo(nombreArchivo.c_str());

	if (archivo.is_open()){

		cout<<"Archivo abierto correctamente"<<endl;
		while(!archivo.eof()){
			string codigo,dato;
			getline(archivo,codigo,';');
			int n[4];
			for(int i=0;i<4;i++){
				char separador;
				if(i<3)separador=';';
				else separador ='\n';
				getline(archivo,dato,separador);
				n[i]=atoi(dato.c_str());
				cout<<"**"<<dato<<"**"<<endl;
			}
		estudiante a;
		a.inicializar(n[0],n[1],n[2],n[3]);
		a.imprimir();


		}
		archivo.close();
	}
}
struct AlumnoLPE{
	string codigo;
	vector<int> notas;

	float promedio;
	void calcularPromedio(){
			promedio=0;
			for(int i=0;i<notas.size();i++){
				promedio+=notas[i];
			}
			promedio=promedio/notas.size();
		}
	void agregarNota(int n){
		notas.push_back(n);
		calcularPromedio();
	}
	void inicializar(string cod){
		codigo=cod;
	}
	void imprimir(){
		cout<<"Alumno "<<codigo<<" tiene promedio: "<<promedio<<endl;
	}
	string obtenerRepresentacion(string separador){
			stringstream respuesta;
			respuesta<<codigo;
			respuesta<<separador;
			for(int i=0;i<notas.size();i++){
				respuesta<<notas[i];
				respuesta<<separador;
			}
			respuesta<<promedio;
			return respuesta.str();
		}
};
void pruebaEstructuraLPE(){
	struct AlumnoLPE a;
	a.inicializar("20160303F");
	a.agregarNota(15);
	a.imprimir();
	a.agregarNota(19);
	a.imprimir();

}
void pruebaEstructuraLPE50(){
	struct AlumnoLPE a;
	a.inicializar("20160303F");
	a.agregarNota(15);
	cout<<a.obtenerRepresentacion("|");
	a.agregarNota(19);
	cout<<a.obtenerRepresentacion(";");
	a.agregarNota(11);
	cout<<a.obtenerRepresentacion("##");

}
void leerArchivoAlumnoLPE(){
	string nombreArchivo= "archivo1.csv";
	ifstream archivo(nombreArchivo.c_str());

	if (archivo.is_open()){

		cout<<"Archivo abierto correctamente"<<endl;
		while(!archivo.eof()){
			string dato;
			getline(archivo,dato);
			stringstream s(dato);
			bool codigoLeido=false;
			AlumnoLPE a;

			while(!s.eof()){
				getline(s,dato,';');
				if(!codigoLeido){
					a.inicializar(dato);
					codigoLeido=true;
				}else{
					a.agregarNota(atoi(dato.c_str()));
				}
			}
		a.imprimir();
		codigoLeido=true;

		}
		archivo.close();
	}
}
void escribirArchivoAlumnoLPE(vector<struct AlumnoLPE>a,string archivo){
	for(int i=0;i< a.size();i++){
		cout<<a[i].obtenerRepresentacion("|")<<endl;
	}
}
void escribirArchivoAlumnoLPE56(vector<struct AlumnoLPE>a,string archivo){
	ofstream arc;
	arc.open(archivo.c_str(),ios::out);
	if(!arc.bad()){
		for(int i=0;i< a.size();i++){
				cout<<a[i].obtenerRepresentacion("|")<<endl;
			}
	}else{
		cout<<"error al leer archivo";
	}
	arc.close();

}
void escribirArchivoAlumnoLPE59(vector<struct AlumnoLPE>a,string archivo){
	ofstream arc;
	arc.open(archivo.c_str(),ios::out);
	if(!arc.bad()){
		for(int i=0;i< a.size();i++){
				arc<<a[i].obtenerRepresentacion("|")<<endl;
			}
	}else{
		cout<<"error al leer archivo";
	}
	arc.close();

}
void leerArchivoAlumnoLPE52(){
	vector<struct AlumnoLPE> v;

	string nombreArchivo= "archivo1.csv";
	ifstream archivo(nombreArchivo.c_str());

	if (archivo.is_open()){

		cout<<"Archivo abierto correctamente"<<endl;
		while(!archivo.eof()){
			string dato;
			getline(archivo,dato);
			stringstream s(dato);
			bool codigoLeido=false;
			AlumnoLPE a;

			while(!s.eof()){
				getline(s,dato,';');
				if(!codigoLeido){
					a.inicializar(dato);
					codigoLeido=true;
				}else{
					a.agregarNota(atoi(dato.c_str()));
				}
			}
		a.imprimir();
		codigoLeido=true;
		v.push_back(a);
		}
		escribirArchivoAlumnoLPE59(v,"alumno2.dat");
		archivo.close();
	}
}

int main() {

	ofstream archivo;
	archivo.open("archivo1.txt");
	archivo<<"Hola"<<endl;
	archivo.close();
	typedef struct estudiante estudiante;
	//leerArchivoAlumno();
	//pruebaEstructuraLPE();
	//leerArchivoAlumnoLPE();
	//pruebaEstructuraLPE50();
	leerArchivoAlumnoLPE52();
	return 0;

}

