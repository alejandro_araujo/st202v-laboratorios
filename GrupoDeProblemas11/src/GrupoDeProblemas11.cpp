
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <fstream>
#include <sstream>
#include <cstring>

using namespace std;
struct texto{
	string palabra;
	struct texto *siguiente;
	struct texto *anterior;
};
struct cliente{
	int orden;
	int cod;
	float tip;
	float num;
	string nombre;
	struct cliente *siguiente;
};
struct libro{
	int idLibro;
	string ISBN;
	int idAutor;
	string fecha;
	struct libro *siguiente;
};
float volumenCono(int radio, int altura);
void Problema01();
void problema03();
float desviacion(int valores[],int n);
int **obtenerMatrizAleatorio(int filas,int columnas);
void imprimirMatriz(int **matriz, int filas, int columnas);
void Problema05();

int factorial(int n){
	int rpt=1;
	for(int i=1;i<=n;i++){
		rpt=rpt*i;
	}
	return rpt;
}
void aprox(float epsilon){
	float e=1;
	int i=1;
	for(i=1;i<17;i++){

		e=e+1.0*1/factorial(i);
	}
	cout<<e;
}
int main() {

	//Problema01();
	//problema03();
	//aprox(1);no entendi muy bien la parte del epsilon :(
	Problema05();
	return 0;
}
//Problema01
float volumenCono(int radio, int altura){
	return (radio*radio*altura*3.1416)/3;
}
void Problema01(){
	int radio,altura;
	do{
		cin>>radio;
		cin>>altura;
	}while(radio<=0 and altura<=0);
	cout<<"el volumen del cono es: "<<volumenCono(radio,altura)<<endl;
}

//Problema03
float desviacion(int valores[],int n){
	int acumulado=0;
	for(int i=0;i<n;i++){
		acumulado=acumulado+valores[i];
	}
	float promedio=1.0*acumulado/n;
	cout<<promedio<<endl;
	float acumulado2=0;
	for(int i=0;i<n;i++){
		acumulado2=acumulado2+(promedio-valores[i])*(promedio-valores[i]);
	}
	cout<<"suma de cuadrados es: "<<acumulado2<<endl;
	acumulado2=1.0*acumulado/n;
	float rpt=pow(acumulado2,0.5);
	return rpt;
}
void problema03(){
	int valores[10]={1,2,3,4,5,6,7,8,9,10};
	cout<<"La desviacion estandar de 1,2,3,4,5,6,7,8,9,10 es: "<<desviacion(valores,10)<<endl;
}
//Problema05
//funciones de apoyo del grupo de problemas 08
void imprimirMatriz(int **matriz, int filas, int columnas){

	for(int i=0;i<filas;i++){
		for(int j=0;j<columnas;j++){
			cout<<*(*(matriz+i)+j)<<" ";
		}
		cout<<endl;
	}
}
int **obtenerMatrizAleatorio(int filas,int columnas){

	int **matriz =new int*[filas];
	int **matrizPtr = matriz;

	int valor = 0;

	for(int i=0;i<filas;i++){
		*(matrizPtr+i) = new int[columnas];
		for(int j=0;j<columnas;j++){
			valor=rand()/RAND_MAX*50;
		}
	}
	return matriz;
}
void Problema05(){
	int **matriz=obtenerMatrizAleatorio(5,5);
	imprimirMatriz(matriz,5,5);
}
//Problema07
int **obtenerMatriz(int filas,int columnas){

	int **matriz =new int*[filas];
	int **matrizPtr = matriz;

	int valor = 0;

	for(int i=0;i<filas;i++){
		*(matrizPtr+i) = new int[columnas];
		for(int j=0;j<columnas;j++){
			valor=rand()/RAND_MAX*50;
		}
	}
	return matriz;
}
void Problema07(){
	int **inicio=obtenerMatriz(5,5);
	int filas=5;
	int columnas=5;
	int acumulados[5]={0};
	for(int i=0;i<filas;i++){
			for(int j=0;j<columnas;j++){
				acumulados[i]=acumulados[i]+*(*(inicio+i)+j);
			}
	}
	for(int i=0;i<filas;i++){
		cout<<"el total en el anio "<<i<<" es igual a :"<<acumulados[i];
	}
}
//Problema09
libro* crear(){
	libro* inicio =new libro();
	inicio->ISBN="";
	inicio->idLibro=0;
	inicio->idAutor=0;
	inicio->fecha="";
	inicio->siguiente=NULL;
	return inicio;
}
void eliminarLibro(libro *inicio, int posicion){
	libro *libroeliminar=NULL;
	libro *tmp=inicio;
	for(int i=0;i<posicion;i++){
		tmp=tmp->siguiente;
	}
	libroeliminar=tmp->siguiente;
	tmp->siguiente=libroeliminar->siguiente;
	delete libroeliminar;
}
//entedi actualizar como agregar
void actualizarLibro(libro *inicio,int posicion){
	libro* nuevoLibro=new libro();
	cin>>nuevoLibro->ISBN;
	cin>>nuevoLibro->fecha;
	cin>>nuevoLibro->idAutor;
	cin>>nuevoLibro->idLibro;
	libro *tmp=inicio;
	for(int i=0;i<posicion;i++){
		tmp=tmp->siguiente;
	}
	nuevoLibro->siguiente=tmp->siguiente;
	tmp->siguiente=nuevoLibro;
}
void leerLibro(libro *inicio,int posicion){
	libro *tmp=inicio;
	for(int i=0;i<posicion;i++){
		tmp=tmp->siguiente;
	}
	cout<<tmp->ISBN<<endl;
	cout<<tmp->fecha<<endl;
	cout<<tmp->idAutor<<endl;
	cout<<tmp->idLibro<<endl;
}
//Problema10
void agregarLibroOrdenado(libro *inicio,int idLibro){
	libro *nuevoLibro=new libro();
	libro *tmp=inicio;
	libro *anterior=tmp;
	int valorLista;
	while(true){

		valorLista=tmp->idLibro;
		if(valorLista>idLibro){
			tmp=anterior;
			break;
		}
		if(tmp->siguiente==NULL){
			break;
		}
		tmp=tmp->siguiente;
		anterior->siguiente=tmp;
	}
	nuevoLibro->siguiente=tmp->siguiente;
	cin>>nuevoLibro->ISBN;
	cin>>nuevoLibro->fecha;
	cin>>nuevoLibro->idAutor;
	cin>>nuevoLibro->idLibro;
	tmp->siguiente=nuevoLibro;
}
//Problema13
//codigo obtenido de la pagina de Programacion ATS para la implementacion de una cola,
//as� como tambi�n poder agregar(registrar) y eliminar(atender) elementos a a la cola
bool colaVacia(cliente *frente){
	return (frente==NULL)? true: false;
}
//parte b
void insertarCola(struct cliente *frente, struct cliente *fin, int orden, int cod, string nombre,float tip, float num){
	cliente *nuevoNodo=new cliente();
	nuevoNodo->cod=cod;
	nuevoNodo->nombre=nombre;
	nuevoNodo->num=num;
	nuevoNodo->orden=orden;
	nuevoNodo->tip=tip;
	nuevoNodo->siguiente=NULL;
	if(colaVacia(frente)){
		frente=nuevoNodo;
	}else{
		fin->siguiente=nuevoNodo;
	}
	fin=nuevoNodo;
	cout<<nuevoNodo->cod;
	cout<<nuevoNodo->nombre;
	cout<<nuevoNodo->num;
	cout<<nuevoNodo->orden;
	cout<<nuevoNodo->tip;

}
//parte c
void eliminarElementosCola(struct cliente *frente, struct cliente *fin){
	cliente *auxiliar=frente;
	cout<<"Los datos del cliente atendido son: "<<endl;
	cout<<frente->cod<<endl;
	cout<<frente->nombre<<endl;
	cout<<frente->num<<endl;
	cout<<frente->orden<<endl;
	cout<<frente->tip<<endl;

	if(frente==fin){
		frente=NULL;
		fin==NULL;
	}else{
		frente=frente->siguiente;
	}
	delete auxiliar;
}
void Problema11(){
	cliente *frente=NULL;
	cliente *fin=NULL;

}
//Problema15 del grupo de problemas 10
//en vez de una pila se esta usando una lista doblemente enlazada porque
//en el problema pide leer desde la palabra m�s antigua hasta la m�s reciente
//y eso no se puede lograr con una pila, a lo mas se podr�a
//mostrar las palabras de la m�s reciente a la m�s antigua
texto *crearListaTexto(){
	texto *inicio=new texto();
	inicio->palabra="-1";
	inicio->siguiente=NULL;
	inicio->anterior=NULL;
	return inicio;
}
//parte a
void agregarPalabraInicio(texto *inicio,string palabra){
	texto *nuevoNodo=new texto();
	nuevoNodo->palabra=palabra;
	nuevoNodo->siguiente=inicio->siguiente;
	inicio->siguiente=nuevoNodo;
}
//parte c.ii
void eliminarUltimaPalabra(texto *inicio){
	texto *nodoEliminar=NULL;
	texto *tmp=inicio;
	tmp=tmp->siguiente;
	nodoEliminar=tmp->siguiente;
	tmp->siguiente=nodoEliminar->siguiente;
	delete nodoEliminar;
}
//parte c.i
void verTextoCompleto(texto *inicio){
	texto *tmp=inicio;
	while(tmp!=NULL){
		tmp=tmp->siguiente;
	}
	while(tmp->palabra!="-1"){
		cout<<tmp->palabra<<" "<<endl;
		tmp=tmp->anterior;
	}
}
//parte a
string insertarPalabra(){
	string palabra;
	cout<<"Digite la palabra que desea agregar"<<endl;
	cin>>palabra;
	//parte b, presionar enter para insertar el string "palabra"
	return palabra;
}

void problema15(){
	struct texto *inicio=crearListaTexto();
	string palabra=insertarPalabra();
	cout<<"Opciones"<<endl;
	cout<<"(1)Ver Texto Completo "<<endl;
	cout<<"(2)Deshacer "<<endl;
	cout<<"(3)Continuar Escribiendo "<<endl;
	int opcion;
	cin>>opcion;
	switch(opcion){
	case 1:verTextoCompleto(inicio);
		problema15();
		break;
	case 2:eliminarUltimaPalabra(inicio);
		break;
	case 3:agregarPalabraInicio(inicio,insertarPalabra());
		problema15();
		break;
	}
}


