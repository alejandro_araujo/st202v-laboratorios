//============================================================================
// Name        : Punteros_PD.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <vector>
using namespace std;

void pruebaDirecciones();
void pruebaIncremento();
void aumentos( float* sueldos, float ratio, int n);
void imprimirSueldos(float s[],int n);
void pruebaFuncionesReferencia();
float *obtenerDatosSalarios();
void agregarcomision(struct Trabajador *t,string concepto, float monto);
void probarFuncionEstructuraRef();
struct ConceptoSalario{
	int id;
	string descripcion;
	float monto;
};
struct Trabajador{
	int id;
	string nombre;
	float salarioTotal;
	vector<struct ConceptoSalario> detalleSalario;
};

int main() {
	//pruebaIncremento();
	//pruebaFuncionesReferencia();
	probarFuncionEstructuraRef();
	return 0;
}

void imprimirSueldos(float s[],int n){
	for(int i=0;i<n;i++){
		cout<<"Sueldo "<<i<<": "<<s[i]<<endl;
	}
}
void aumentos( float* sueldos, float ratio, int n){
	for(int i=0;i<n;i++){
		(*sueldos)+=ratio*(*sueldos);
		*sueldos++;
	}

}
float *obtenerDatosSalarios(){
	const int NDATOS=5;
	//float* respuesta= (float*)malloc(sizeof(float)*NDATOS);
	float* respuesta = new float[NDATOS];
	float sueldos[5];
	for(int i=0;i<NDATOS;i++){
		respuesta[i]=1.0* rand()/RAND_MAX*20000;
	}
	return respuesta;
}
void agregarcomision(struct Trabajador *t,string concepto, float monto){
	int ide=t->detalleSalario.size()+1;
	struct ConceptoSalario c;
	c.id=ide;
	c.descripcion=concepto;
	c.monto=monto;
	(*t).detalleSalario.push_back(c);
}
void probarFuncionEstructuraRef(){
	struct Trabajador trabajador;

	trabajador.id=1;
	trabajador.nombre="Juan Perez";
	trabajador.salarioTotal=5000;
	agregarcomision(&trabajador,"inflacion", 0.1* trabajador.salarioTotal);
	cout<<trabajador.detalleSalario[0].descripcion<<endl;
	agregarcomision(&trabajador,"bonificacion",300);
	cout<<trabajador.detalleSalario[1].descripcion<<endl;

}
void pruebaFuncionesReferencia(){
	float *sueldos;
	sueldos=obtenerDatosSalarios();
	imprimirSueldos(sueldos,5);
	aumentos(sueldos,0.1,5);
	imprimirSueldos(sueldos,5);
}
void pruebaIncremento(){
	double promedio=18.5;
	double *puntero=&promedio;
	double *puntero2=&promedio;
	cout<<*puntero<<endl;
	(*puntero)++;
	cout<<*puntero<<endl;
	cout<<*puntero2<<endl;
}

void pruebaDirecciones(){
	int entero;
	float real;
	int vector[4];
	char caracter[120];
	cout<<&entero<<endl;
	cout<<&real<<endl;
	cout<<&vector<<endl;
	cout<<&caracter<<endl;
}
