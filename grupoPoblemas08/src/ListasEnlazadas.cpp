//============================================================================
// Name        : ListasEnlazadas.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
//asdasd
#include <iostream>
#include <string>
using namespace std;
struct nodo{
	int valor;
	struct nodo *siguienteNodo;
};
struct nodo_notas{
	int valor;
	struct nodo_notas *siguiente_nodo;
};
struct nodoDoble{
	int valor;
	struct nodoDoble *siguienteNodo;
	struct nodoDoble *anteriorNodo;
};
struct empleado{
	int idEmpleado;
	int idRol;
	string nombre;
	float salario;
};
void aniadirNodo(nodo *inicio,int valor);
nodo* agregarNodo(nodo *inicio,int valor);
void pruebaNodos();
void agregarNodoPosicion(nodo *inicio,int valor, int posicion);
void eliminarNodoPosicion(nodo *inicio,int posicion, int valor);
void imprimir(nodo* p, int n);
nodo* last(nodo* inicio,int ultimaPosicion);
void imprimirInverso(nodo *inicio, int posicion);
int cantElementos(nodo *inicio);
nodoDoble *crearListaDoble();
void imprimirValoresListaDoble(nodoDoble *inicio);
void agregarNodoInicio(nodoDoble *inicio,int valor);
void agregarNodoPosicion( nodoDoble *inicio,int posicion, int valor);
//######################################
int main() {


	return 0;
}
//######################################
void aniadirNodo(nodo *inicio,int valor){
	nodo *nuevoNodo= new nodo();
	nuevoNodo->valor=valor;
	nuevoNodo->siguienteNodo=inicio->siguienteNodo;
	inicio->siguienteNodo=nuevoNodo;
}
void crearNodoInicio(nodo *inicio,int valor){
	nodo *nuevoNodo= new nodo();
	nuevoNodo->valor=valor;
	nuevoNodo->siguienteNodo=inicio->siguienteNodo;
	inicio->siguienteNodo=nuevoNodo;
}
void agregarNodoPosicion(nodo *inicio,int valor, int posicion){
	nodo *nuevoNodo= new nodo();
	nuevoNodo->valor=valor;
	nodo *temp=inicio;
	for( int i=0;i<posicion;i++){
		temp= temp->siguienteNodo;
	}
	nuevoNodo->siguienteNodo=temp->siguienteNodo;
	temp->siguienteNodo=nuevoNodo;
}
void eliminarNodoPosicion(nodo *inicio,int posicion, int valor){
	nodo *nodoEliminar=NULL;
	nodo *temp=inicio;
	for(int i=0;i<posicion;i++){
		temp=temp->siguienteNodo;
	}
	nodoEliminar=temp->siguienteNodo;
	temp->siguienteNodo=nodoEliminar->siguienteNodo;
}
//Problema01
nodo* agregarNodo(nodo *inicio, int valor){
	nodo *nuevoNodo= new nodo();
	nuevoNodo->valor=valor;

	nuevoNodo->siguienteNodo=inicio->siguienteNodo;
	inicio->siguienteNodo=nuevoNodo;
	return nuevoNodo;

}
//Problema02
void imprimir(nodo* p, int n){
	for(int i=0;i<n;i++){
		cout<<"el  valor "<<i<<" del nodo es: "<<(*p).valor<<endl;
		p=p->siguienteNodo;
	}
}
//Problema03
nodo* last(nodo* inicio,int ultimaPosicion){
	nodo *temp=inicio;
	for(int i=0;i<ultimaPosicion;i++){
		temp=temp->siguienteNodo;
	}
	return temp;
}
//Problema04
void imprimirInverso(nodo *inicio, int posicion){
	nodo *temp=inicio;
	int valores[posicion];
	for(int i=0;i<posicion;i++){
		valores[i]=temp->valor;
		temp=temp->siguienteNodo;
	}
	for(int i=posicion-1;i>=0;i--){
		cout<<"los valores en orden de retroceso son:"<<valores[i]<<endl;
	}
}
//Problema05
int cantElementos(nodo *inicio){
	int cantidad=0;
	nodo *start=inicio;
	while(start!=NULL){
		cantidad=cantidad+1;
		start=start->siguienteNodo;
	}
	return cantidad;
}
void pruebaNodos(){
	struct nodo nodito;
	nodito.siguienteNodo=NULL;
	aniadirNodo(&nodito,5);
	cout<<"el valor del nodito es: "<< (*nodito.siguienteNodo).valor <<endl;
	struct nodo* nodazo;
	nodazo=agregarNodo(&nodito,3);
	nodazo=agregarNodo(&nodito,4);
	nodazo=agregarNodo(&nodito,3);
	nodazo=agregarNodo(&nodito,2);
	nodazo=agregarNodo(&nodito,1);
	imprimir(nodazo,5);
	cout<<"el ultimo elemento de la lista es: "<<(*last(nodazo,5)).valor<<endl;
	imprimirInverso(nodazo,6);
	cout<<"la cantidad de elementos del nodo es: "<<cantElementos(nodazo)<<endl;
}

nodoDoble *crearListaDoble(){
	nodoDoble *inicio=new nodoDoble();
	inicio->valor=-1;
	inicio->anteriorNodo=NULL;
	inicio->siguienteNodo=NULL;
	return inicio;
}

void imprimirValoresListaDoble(nodoDoble *inicio){
	 cout<<"**"<<endl;
	while(inicio->siguienteNodo!=NULL){
		cout<<inicio->valor<<endl;
		inicio=inicio->siguienteNodo;
	}
	 cout<<"**"<<endl;
}
void agregarNodoInicio(nodoDoble *inicio,int valor){
	nodoDoble *nuevoNodo=new nodoDoble();
	nuevoNodo->siguienteNodo=inicio->siguienteNodo;
	nuevoNodo->valor=valor;
	nuevoNodo->anteriorNodo=inicio;
	if(nuevoNodo->siguienteNodo==NULL){
		nuevoNodo->siguienteNodo->anteriorNodo=nuevoNodo;
	}
	inicio->siguienteNodo=nuevoNodo;
}
void agregarNodoPosicion( nodoDoble *inicio,int posicion, int valor){
	nodoDoble *nuevoNodo=new nodoDoble();
	nuevoNodo->valor=valor;
	nodoDoble *tmp=inicio;

	for(int i=0;i<posicion;i++){
		tmp=tmp->siguienteNodo;
	}
	nuevoNodo->siguienteNodo=tmp->siguienteNodo;
	nuevoNodo->anteriorNodo=tmp;
	tmp->siguienteNodo->anteriorNodo=nuevoNodo;
	tmp->siguienteNodo=nuevoNodo;
}
void eliminarNodo(nodoDoble *inicio,int posicion){
	nodoDoble* nodoEliminar;
	nodoDoble *tmp=inicio;
	for(int i=0;i<posicion;i++){
		tmp=tmp->siguienteNodo;
	}
	nodoEliminar=tmp->siguienteNodo;
	tmp->siguienteNodo=nodoEliminar->siguienteNodo;
	delete nodoEliminar;
}

void agregarNodoOrdenado(nodoDoble *inicio,int valorNuevo){
	nodoDoble *nuevoNodo=new nodoDoble();
	nuevoNodo->valor=valorNuevo;
	nodoDoble *tmp=inicio;
	int valorLista;
	while(true){
		valorLista=tmp->valor;
		if(valorLista>valorNuevo){
			tmp=tmp->siguienteNodo;
			break;
		}
		if(tmp->siguienteNodo==NULL){
			break;
		}
		tmp=tmp->siguienteNodo;
	}
	nuevoNodo=tmp->siguienteNodo;
	nuevoNodo->anteriorNodo=tmp;
	if(tmp->siguienteNodo!=NULL){
		tmp->siguienteNodo->anteriorNodo=nuevoNodo;
	}
	tmp->siguienteNodo=nuevoNodo;
}
